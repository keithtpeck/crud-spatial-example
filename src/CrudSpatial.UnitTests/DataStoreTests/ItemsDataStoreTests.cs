using System;
using Xunit;
using CrudSpatial.Data;
using CrudSpatial.Models;
using Microsoft.EntityFrameworkCore;
using FluentAssertions;
using System.Threading.Tasks;
using System.Linq;

namespace CrudSpatial.UnitTests.DataStoreTests
{
    public class ItemsDataStoreTests : IDisposable
    {
        public void Dispose(){
            db.Dispose();
        }
        
        private DataContext db;

        public ItemsDataStoreTests(){

            var options = new DbContextOptionsBuilder<DataContext>()
            .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
            .Options;

            db = new DataContext(options);
        }
        
        [Fact]
        public async Task should_return_item()
        {
            db.Add(new Item{
                Id = 1
            });

            db.SaveChangesAsync();

            var store = new ItemsDataStore(db);

            var item = await store.GetItemAsync(1);

            item.Should().NotBeNull();
        }

        [Fact]
        public async Task should_return_null_item()
        {
            var store = new ItemsDataStore(db);

            db.Add(new Item{
                Id = 1
            });

            db.SaveChangesAsync();

            // Getting item 2 which does not exist
            var item = await store.GetItemAsync(2);

            item.Should().BeNull();
        }

        [Fact]
        public async Task should_add_item()
        {
            var store = new ItemsDataStore(db);

            var item = new Item{
                Id = 1,
                Name = "test name"
            };

            await store.CreateItemAsync(item);

            db.Items.First().Should().BeEquivalentTo(item);
        }

        
        [Fact]
        public async Task should_update_item()
        {
            var store = new ItemsDataStore(db);

            db.Add(new Item{
                Id = 1,
                Name = "Original",
                CreatedDate = new DateTime(2019,01,01)
            });

            db.SaveChangesAsync();

            var item = new Item{
                Id = 1,
                Name = "Updated",
                UpdatedDate = new DateTime(2020,01,01)
            };

            store.UpdateItemAsync(item);

            db.Items.First().Should().BeEquivalentTo(new Item{
                Id = 1,
                Name = "Updated",
                CreatedDate = new DateTime(2019,01,01),
                UpdatedDate = new DateTime(2020,01,01)
            });
        }

        [Fact]
        public async Task should_delete_item()
        {
            var store = new ItemsDataStore(db);

            db.Add(new Item{
                Id = 1,
                Name = "Name",
                CreatedDate = new DateTime(2019,01,01)
            });

            db.SaveChangesAsync();

            await store.DeleteItemAsync(1);

            db.Items.Count().Should().Be(0);
        }
    }
}
