using Xunit;
using CrudSpatial.Controllers;
using CrudSpatial.Data;
using CrudSpatial.Models;
using CrudSpatial;
using FluentAssertions;
using System.Threading.Tasks;
using FluentValidation;
using FakeItEasy;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using FluentValidation.Results;
using GeoJSON.Net.Geometry;

namespace CrudSpatial.UnitTests.DataStoreTests
{
    public class ItemsControllerTests
    {
        private IItemsDataStore store;
        private IValidator<Item> validator;
        private IDateTime dateTime;
        private ItemsController controller;

        public ItemsControllerTests(){
            validator = A.Fake<IValidator<Item>>();
            store = A.Fake<IItemsDataStore>();
            dateTime = A.Fake<IDateTime>();

            controller = new ItemsController(store, validator, dateTime);
        }
        
        // Get

        [Fact]
        public async Task should_return_okay_when_getting_items()
        {
            var items = new List<Item>{
                new Item{
                    Id = 1,
                    Name = "Name1",
                    Location = new GeoJSON.Net.Geometry.Point(new Position(51.899, -2.124))
                },
                new Item{
                    Id = 2,
                    Name = "Name2",
                    Location = new GeoJSON.Net.Geometry.Point(new Position(1.897, -1.156))
                }
            };

            A.CallTo(() => store.GetItemsAsync()).Returns(items);

            var actionResult = await controller.Get();

            // Check 200 response
            var modelResult = Assert.IsType<OkObjectResult>(actionResult.Result);
            
            // Check matches expected model
            modelResult.Value.Should().BeEquivalentTo(new CollectionResult<Item>(items, items.Count));
        }


        [Fact]
        public async Task should_return_not_found_when_getting_item_which_does_not_exist()
        {
            A.CallTo(() => store.GetItemAsync(1)).Returns((Item)null);
            
            var actionResult = await controller.Get(1);

            A.CallTo(() => store.GetItemAsync(1)).MustHaveHappened();

            // Check 404 response
            Assert.IsType<NotFoundResult>(actionResult.Result);            
        }

        // Post
     
        [Fact]
        public async Task should_return_created_when_sucessfully_posting_item()
        {            
            // Setup validation to return valid result
            A.CallTo(() => validator.Validate(null)).WithAnyArguments().Returns(new FluentValidation.Results.ValidationResult{
                
            });

            var date = new System.DateTime(2020,01,01);
            A.CallTo(() => dateTime.UtcNow).Returns(date);

            A.CallTo(() => store.CreateItemAsync(new Item{
                Id = 0,
                Name = "Name",
                CreatedDate = date
            })).WithAnyArguments().Returns(new Item{
                Id = 1,
                Name = "Name",
                CreatedDate = date
            });

            var actionResult = await controller.Post(new Item{
                Name = "Name"
            });

            // Check 201 response
            var modelResult = Assert.IsType<CreatedResult>(actionResult);

            modelResult.Value.Should().BeEquivalentTo(new Item{
                Id = 1,
                Name = "Name",
                CreatedDate = date
            });
        }

        [Fact]
        public async Task should_return_bad_request_when_posting_and_validation_fails()
        {            
            A.CallTo(() => validator.Validate(null)).WithAnyArguments().Returns(new FluentValidation.Results.ValidationResult(new List<ValidationFailure>{new ValidationFailure("name","name required")}));

            var actionResult = await controller.Post(new Item{
                Id = 1,
                Name = "New Name"
            });

            // TODO further checking of response should be done here
            var modelResult = Assert.IsType<BadRequestObjectResult>(actionResult);
        }

        // TODO further tests for remaining endpoints        
    }
}
