CREATE TABLE items
(
    "Id" SERIAL PRIMARY KEY,
    "Location" text,
    "Name" text NOT NULL,
    "CreatedDate" timestamp with time zone NOT NULL,
    "UpdatedDate" timestamp with time zone
)