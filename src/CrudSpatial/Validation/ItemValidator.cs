﻿using CrudSpatial.Models;
using FluentValidation;

namespace CrudSpatial.Validation
{
    public class ItemValidator : AbstractValidator<Item>
    {
        public ItemValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
        }
    }
}
