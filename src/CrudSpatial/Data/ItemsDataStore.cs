﻿using CrudSpatial.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CrudSpatial.Data
{
    public interface IItemsDataStore
    {
        Task<Item> GetItemAsync(int id);
        Task<List<Item>> GetItemsAsync();
        Task UpdateItemAsync(Item item);
        Task<Item> CreateItemAsync(Item item);
        Task DeleteItemAsync(int id);
    }

    public class ItemsDataStore : IItemsDataStore
    {
        private readonly DataContext dataContext;

        public ItemsDataStore(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public async Task<Item> CreateItemAsync(Item item)
        {
            dataContext.Items.Add(item);
            await dataContext.SaveChangesAsync();

            // Item will now have an id assigned
            return item;
        }

        public async Task DeleteItemAsync(int id)
        {
            var existing = await dataContext.Items.FirstOrDefaultAsync(i => i.Id == id);
            if (existing == null)
            {
                // We shouldn't get to this as object existance is checked in controller
                throw new Exception($"Item with Id '{id}' could not be found in database.");
            }

            dataContext.Items.Remove(existing);
            await dataContext.SaveChangesAsync();
        }

        public async Task<Item> GetItemAsync(int id)
        {
            return await dataContext.Items.FirstOrDefaultAsync(i => i.Id == id);
        }

        public async Task<List<Item>> GetItemsAsync()
        {
            return  await dataContext.Items.ToListAsync();
        }

        public async Task UpdateItemAsync(Item item)
        {
            var existing = await dataContext.Items.FirstOrDefaultAsync(i => i.Id == item.Id);
            if(existing == null)
            {
                // We shouldn't get to this as object existance is checked in controller
                throw new Exception($"Item with Id '{item.Id}' could not be found in database.");
            }

            existing.Name = item.Name;
            existing.UpdatedDate = item.UpdatedDate;
            existing.Location = item.Location;

            await dataContext.SaveChangesAsync();
        }
    }
}
