﻿using CrudSpatial.Models;
using GeoJSON.Net.Geometry;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace CrudSpatial.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Item> Items { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>(item =>
            {
                item.ToTable("items")
                // Read only prop
                .Ignore(i => i.GoogleMapsLink)
                .Property(i => i.Location)
                    .HasConversion(
                        // Convert Location into JSON when saving to database
                        location => location == null ? null : JsonConvert.SerializeObject(location),
                        // Deserialize JSON into Location object when retrieving from database 
                        location => location == null ? null : JsonConvert.DeserializeObject<Point>(location));
                }       
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}