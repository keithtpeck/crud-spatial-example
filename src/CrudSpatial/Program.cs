﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CrudSpatial
{
    public class Program
    {
        public const string ConnectionStringEnvName = "POSTGRESQL_CONN";
        public static string ConnectionString => Environment.GetEnvironmentVariable(ConnectionStringEnvName);

        public static void Main(string[] args)
        {
            if (string.IsNullOrWhiteSpace(ConnectionString))
            {
                throw new Exception($"PostgreSQL connection string '{ConnectionStringEnvName}' environment variable required.");
            }

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
