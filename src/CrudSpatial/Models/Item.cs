﻿using System;
using GeoJSON.Net;

namespace CrudSpatial.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public GeoJSON.Net.Geometry.Point Location { get; set; }
        public string GoogleMapsLink => Location == null || Location.Type != GeoJSONObjectType.Point ? null : $"http://maps.google.com/maps?q={Location.Coordinates.Longitude},{Location.Coordinates.Latitude}";
    }
}
