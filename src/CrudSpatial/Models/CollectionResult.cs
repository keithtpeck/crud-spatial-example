﻿using System.Collections.Generic;

namespace CrudSpatial.Models
{
    /// <summary>
    /// Used so that paging could be implemented in future using 'count' without breaking API changes
    /// </summary>
    public class CollectionResult<T>
    {
        public List<T> Results { get; set; }
        public int Count { get; set; }

        public CollectionResult() { }

        public CollectionResult(List<T> results, int count)
        {
            Results = results;
            Count = count;
        }
    }
}
