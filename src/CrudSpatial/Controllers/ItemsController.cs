﻿using System;
using System.Threading.Tasks;
using CrudSpatial.Data;
using CrudSpatial.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;

namespace CrudSpatial.Controllers
{
    [Route("api/items")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IItemsDataStore store;
        private readonly IValidator<Item> validator;
        private readonly IDateTime dateTime;

        public ItemsController(IItemsDataStore itemsDataStore, IValidator<Item> validator, IDateTime dateTime)
        {
            this.store = itemsDataStore;
            this.validator = validator;
            this.dateTime = dateTime;
        }

        [HttpGet]
        public async Task<ActionResult<CollectionResult<Item>>> Get()
        {
            var items = await store.GetItemsAsync();
            
            // returns a 200
            return Ok(new CollectionResult<Item>(items, items.Count));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Item>> Get(int id)
        {
            var item = await store.GetItemAsync(id);
            if(item == null)
            {
                // Returns 404
                return NotFound();
            }

            // returns a 200
            return Ok(item);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Item model)
        {
            model.CreatedDate = dateTime.UtcNow;
            model.Id = 0;

            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
            {
                // Returns 400
                return BadRequest(validationResult.Errors);
            }

            var item = await store.CreateItemAsync(model);
            
            // Returns a 201
            return Created(uri:$"/api/items/{item.Id}", item);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<Item>> Put(int id, [FromBody] Item model)
        {
            var item = await store.GetItemAsync(id);
            if (item == null)
            {
                // Returns 404
                return NotFound();
            }

            var validationResult = validator.Validate(model);
            if (!validationResult.IsValid)
            {
                // Returns 400
                return BadRequest(validationResult.Errors);
            }

            item.Location = model.Location;
            item.Name = model.Name;
            item.UpdatedDate = dateTime.UtcNow;

            await store.UpdateItemAsync(item);

            // Returns 200. Could return an empty body with a 204 here but often useful to return updated object to client 
            return Ok(item);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var item = await store.GetItemAsync(id);
            if (item == null)
            {
                // Returns 404
                return NotFound();
            }

            await store.DeleteItemAsync(id);

            // Returns a 204
            return new NoContentResult();
        }
    }
}
