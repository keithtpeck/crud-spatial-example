# CRUD Spatial Example

A C# ASP.NET Core 2.1 application to demonstrate an API and use of GeoJSON type.

## Hosted version

[http://crud-lb-2054814613.us-east-2.elb.amazonaws.com](http://crud-lb-2054814613.us-east-2.elb.amazonaws.com)

## Running in Docker

Create table in PostgeSQL with the following schema:

```
CREATE TABLE items
(
    "Id" SERIAL PRIMARY KEY,
    "Location" text,
    "Name" text NOT NULL,
    "CreatedDate" timestamp with time zone NOT NULL,
    "UpdatedDate" timestamp with time zone
)
```

Run Docker command (image is a Linux build)

```
docker run -e="POSTGRESQL_CONN={your connection string}" -p 8080:80 keithpeck/crud-spatial:latest
``` 

## Usage
Swagger API documentation at root should make endpoints easy to understand. But as an example:

```
GET: http://localhost:8080/api/items
```
```
POST: http://localhost:8080/api/items 
{
    "name": "London",
    "location": {
        "type": "Point",
        "coordinates": [
            51.507546, -0.119507
        ]
    }
}
```
